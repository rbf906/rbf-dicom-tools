### SET FOLDER TO WATCH + FILES TO WATCH + SUBFOLDERS YES/NO
    $watcher = New-Object System.IO.FileSystemWatcher
    $watcher.Path = "C:\data\mriphysics_aet"
    $watcher.Filter = "*.*"
    $watcher.IncludeSubdirectories = $true
    $watcher.EnableRaisingEvents = $true
    $listPath = ".\transfer_list.txt"
    $inProgressPath = ".\transfers_to_do.txt"
    if (Test-Path -Path $listPath -PathType Leaf) {
        Remove-Item -Path $listPath
    }
    if (Test-Path -Path $inProgressPath -PathType Leaf) {
        Remove-Item -Path $inProgressPath
    }

### DEFINE ACTIONS AFTER AN EVENT IS DETECTED
    $action = { $path = $Event.SourceEventArgs.FullPath
                $changeType = $Event.SourceEventArgs.ChangeType
                $logline = "$path"
                if (Test-Path -Path $path -PathType Leaf) {
                    Add-Content $listPath -value $logline
                }
    }
### DECIDE WHICH EVENTS SHOULD BE WATCHED
    Register-ObjectEvent $watcher "Created" -Action $action
#    Register-ObjectEvent $watcher "Changed" -Action $action
#    Register-ObjectEvent $watcher "Deleted" -Action $action
#    Register-ObjectEvent $watcher "Renamed" -Action $action
    $prevLastFile = ""
    while ($true) {
        Wait-Event -Timeout 30
        if (Test-Path -Path $listPath -PathType Leaf) {
            $lastFile = Get-Content $listPath -Tail 1

        } else {
            $prevLastFile = ""
            Write-Host ">>> NO FILES"
            continue
        }
        #Write-Host $prevLastFile
        #Write-Host $lastFile
        if ($prevLastFile -eq $lastFile){
            Write-Host ">>> SORTING"
            Rename-Item -Path $listPath -NewName $inProgressPath
            python rpacs_sort.py $inProgressPath
            Remove-Item $inProgressPath
        } else {
            Write-Host ">>> TRANSFER IN PROGRESS"
            $prevLastFile = $lastFile
        }
    }
