import os
import pydicom
from easygui import diropenbox
from subprocess import call
import sys
from uuid import uuid4
import hashlib
import csv

#### MAKE SURE THESE DETAILS ARE CORRECT ############
rpacs_ip = '192.168.14.116'
rpacs_port = '4104'
rpacs_aet = 'ROUTER'
storescu_path = r'C:\data\apps\dcmtk\bin\storescu.exe'


#### Set options for running storescu #############
# "options" requires a leading and trailing space for concatenating
# with other components of the command
options = ' -aet RFPYTHON -aec {} {} {} '.format([rpacs_aet,rpacs_ip,rpacs_port])

# Set temp folder for creating files with modified tags
temp = os.path.join(os.getenv("TEMP"),'python_temp')
temppath = os.path.join(temp,'dcm_instance.dcm')
if not os.path.exists(temp):
    os.makedirs(temp)




# Global variable to support accession number generation
study_uids = {}
# Counting variable to give some clue of how far it's got...
count = 0

def generate_accession(string_length=16):
    new_uid = uuid4().hex
    m = hashlib.md5()
    m.update(new_uid.encode())
    accession = m.hexdigest()[-string_length:]
    return accession.upper()

def send_all(input_path,preamble,newstudy,mappings=None,forced_id=None,generate_accessions=False):
    global count
    file_list = os.listdir(input_path)
    for f in file_list:
        fullpath = os.path.join(input_path, f)
        #~ print '\n'+fullpath
        if not os.path.exists(fullpath):
            print("...File doesn't exist")
            continue
        if os.path.isdir(fullpath):
            send_all(fullpath,preamble,newstudy,mappings,forced_id,generate_accessions)
            print("...Directory found")
            continue

        ds = None
        use_temp = False
        try:
            ds = pydicom.dcmread(fullpath)
        except:
            print("...Not a DICOM file")
            continue
        if ds is None:
            continue
        if ds.file_meta.MediaStorageSOPClassUID.name == 'Media Storage Directory Storage':
            # Skip over DICOMDIR files
            continue
        if not mappings is None:
            print(ds.PatientID,mappings[ds.PatientID])
            ds.PatientID = mappings[ds.PatientID]
            use_temp = True
            # print("From mapping",ds.PatientID)
        if not forced_id is None:
            ds.PatientID = forced_id
            use_temp = True
            # print("From force",ds.PatientID)

        if generate_accessions:
            if not ds.StudyInstanceUID in study_uids.keys():
                this_accession = generate_accession()
                study_uids[ds.StudyInstanceUID] = this_accession
            ds.AccessionNumber = study_uids[ds.StudyInstanceUID]
            use_temp = True


        if not newstudy=="":
            # ds.StudyDescription = "RRK{}".format(newstudy)+" "+ds.StudyDescription
            ds.BranchOfService = "RRK15{}".format(newstudy)
            if 'StudyDescription' in dir(ds):
                ds.StudyDescription = "RRK{}".format(newstudy)+'_'+ds.StudyDescription
            else:
                ds.StudyDescription = "RRK{}".format(newstudy)
            use_temp = True
        if use_temp:
            ds.save_as(temppath)
            command=preamble+temppath
        else:
            command=preamble+fullpath
        count=count+1
        print("{}:".format(count),command)
        call(command,shell=False)
    return


if __name__=='__main__':
    inpath = diropenbox("Select input directory","INPUT","C:/")
    print(inpath)
    preamble = storescu_path+options

    newstudy = input("What study should this be sent to (4 digits only)? Press Enter to skip and use existing tags: ")

    if not newstudy=="":
        # Check that new study is actually a 4 digit code
        if not len(newstudy)==4 or not newstudy.isnumeric():
            print("Invalid study value specified. Please only use a 4 digit code with no prefix. Exiting program.")
            sys.exit()
        else:
            print("Changing all images to use study {}".format(newstudy))

    mappings = None
    forced_id = None
    generate_accessions = False
    use_mappings = input("Use a mappings CSV file to fudge patient IDs? (yes/no): ")
    if use_mappings.upper()=="YES":
        import csv
        with open('mappings.csv', mode='r') as infile:
            reader = csv.reader(infile)
            mappings = {rows[0]:rows[1] for rows in reader}
    elif use_mappings.upper()=="NO":
        override_id = input("Override all patient IDs with single value? (yes/no): ")
        if override_id.upper()=="YES":
            forced_id = input("Please input the desired patient ID: ")
            if not forced_id=="":
                print("Using forced ID - {}".format(forced_id))
            else:
                print("No ID specified, cancelling")
                sys.exit()
        elif not override_id.upper()=="NO":
            print("Cancelling - you didn't say yes or no.")
            sys.exit()
    else:
        print("Cancelling - you didn't say yes or no.")
        sys.exit()

    accessions = input("Generate new unique accession numbers for studies? (yes/no): ")
    if accessions.upper()=="YES":
        generate_accessions = True
    elif not accessions.upper()=="NO":
        print("Cancelling - you didn't say yes or no.")
        sys.exit()

    yesno = input("\nThis will send ALL DICOM DATA in this directory. Are you absolutely sure? (yes/no): ")

    if not inpath is None:
        if yesno.upper()=="YES":
            # print(inpath)
            # print(preamble)
            # print(newstudy)
            # print(mappings)
            # print(forced_id)
            send_all(inpath,preamble,newstudy,mappings,forced_id,generate_accessions)
        else:
            print("Cancelling - you didn't say yes.")

    # Dump all encoded accession numbers as a text file
    with open(os.path.join(inpath,'accession_numbers.csv'),'w') as outfile:
        w = csv.writer(outfile)
        w.writerows(study_uids.items())
