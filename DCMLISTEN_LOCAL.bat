@ECHO OFF

ECHO ##############################################################
ECHO #
ECHO # Simple DICOM listener for use on PC in MRI physics office
ECHO # at QEHB. Options are largely default options for storescp
ECHO # with sorting by study_uid, storing on the MRI physics
ECHO # network share.
ECHO #
ECHO # This is the LOCAL STORAGE version - BE CAREFUL OF DISK
ECHO # SPACE REQUIREMENTS!!!
ECHO #
ECHO ##############################################################

:: Run Powershell folder watcher
start PowerShell.exe -ExecutionPolicy Bypass -File .\folder_watcher.ps1 

ECHO #
ECHO # RUNNING POWERSHELL FILE WATCHER TO SORT FILES TO M:
ECHO #
ECHO ##############################################################
ECHO #
ECHO # Robert Flintham - 10/09/2020 edited 02/03/2022
ECHO #
ECHO ##############################################################

:: Set variables
SET DCMTKDIR=C:\data\apps\dcmtk\bin
SET DICOM_STORAGE_DIR=C:\data\mriphysics_aet
SET AETITLE=MRIPHYSICS
SET PORT=4104

:: Tell user what is happening
ECHO #
ECHO # Received DICOM objects will be stored in:
ECHO #   %DICOM_STORAGE_DIR%
ECHO #
ECHO # Listening on port %PORT% with AE title %AETITLE%
ECHO #
ECHO ##############################################################

"%DCMTKDIR%\storescp" -d -pm -od "%DICOM_STORAGE_DIR%" -su STUDY -aet %AETITLE% %PORT%