import pydicom
import sys
from os import chdir, getcwd
import numpy as np
from siemens import get_ascconv

def print_tags(seq,depth=0):
    for tag in seq:
        if len(str(tag.value))>50 and not tag.VR=='SQ':
            print('--'*depth,str(tag.value[0:32])+' ...')
        else:
            print('--'*depth,tag)
        if tag.VR=='SQ':
            for seq in tag.value:
                print_tags(seq,depth+1)

sys.argv  #sys.argv[1] is the file to upload

fname = ""

for i in range(len(sys.argv)):
	if i==0:
		continue
	if i==1:
		#~ fname = '\"'+fname+str(sys.argv[i])+'\"'
		fname = fname+str(sys.argv[i])
	else:
		#~ fname = '\"'+fname+" "+str(sys.argv[i])+'\"'
		fname = fname+" "+str(sys.argv[i])

print(fname)
ds1 = pydicom.dcmread(fname,force=True)
if 'SpectroscopyData' in ds1:
	ds1.SpectroscopyData = 0


try:
	csa_header = get_ascconv(ds1)
except:
    csa_header = None
	
print("===========================================")
print("   FILE META HEADER")
print("===========================================")
print(ds1.file_meta)
print("===========================================")
print("   DICOM HEADER")
print("===========================================")


print_tags(ds1)


if 'SpectroscopyData' in ds1:
	print("")
	print("WARNING: Spectroscopy data replaced with '0'")
	print("")

if not csa_header is None:
	print("===========================================")
	print("   Siemens CSA Header")
	print("===========================================")
	for row in csa_header:
		dots = (64-len(str(row[0])))
		if dots<2:
			dots = 2
		print(str(row[0]),' '+'.'*dots+' ',str(row[1]).replace('\"',''))

	
#spec_data = np.array(ds1[0x7fe1,0x1010].value,dtype=np.float64)
#print spec_data
