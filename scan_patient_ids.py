import pydicom
import os
import csv

inpath = r'D:\MRI_DATA\OTHER\MRI_RT_PHILIPS_DEMO'

def list_all(input_path,mappings=[]):
    file_list = os.listdir(input_path)
    for f in file_list:
        fullpath = os.path.join(input_path, f)
        #~ print '\n'+fullpath
        if not os.path.exists(fullpath):
            print("...File doesn't exist")
            continue
        if os.path.isdir(fullpath):
            mappings = list_all(fullpath,mappings=mappings)
            print("...Directory found")
            continue
        try:
            ds = pydicom.dcmread(fullpath)
            if not ds.PatientID in mappings:
                print(ds.PatientID)
                mappings.append(ds.PatientID)
        except:
            raise
            print("Couldn't read file or detect patient ID: {}".format(fullpath))
            continue
    return mappings

mappings = list_all(inpath)
print(mappings)
