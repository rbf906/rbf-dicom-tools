@ECHO OFF

ECHO ##############################################################
ECHO #
ECHO # DICOM listener - BE CAREFUL OF DISK
ECHO # SPACE REQUIREMENTS!!!
ECHO #
ECHO ##############################################################
ECHO #
ECHO # Robert Flintham - 14/12/2020
ECHO #
ECHO ##############################################################

:: Set variables
SET DCMTKDIR=C:\apps\dcmtk\bin
SET DICOM_STORAGE_DIR=D:\MRI_DATA\DCM_RECV
SET AETITLE=RBFDICOM
SET PORT=4111
SET LOGPATH=.\log\logfile.txt

:: Tell user what is happening
ECHO #
ECHO # Received DICOM objects will be stored in:
ECHO #   %DICOM_STORAGE_DIR%
ECHO #
ECHO # Listening on port %PORT% with AE title %AETITLE%
ECHO #
ECHO ##############################################################

:: Refresh the log file and fire up the listener
del %LOGPATH%
ECHO DCMTK STORESCP LOGFILE >> %logpath%
"%DCMTKDIR%\storescp" -d -pm -od "%DICOM_STORAGE_DIR%" -su STUDY -aet %AETITLE% %PORT% >> %logpath%