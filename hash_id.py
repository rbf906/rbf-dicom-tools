import sys
import hashlib
import pyperclip

_hash = hashlib.new('md5')

# Allowedoptions
allowedoptions=['len','append']

options={}
orig_id = ''

# Get options from sys.argv
for i in range(len(sys.argv)):
	if i%2==0:
		continue
	try:
		opt=sys.argv[i][2:]
		val=sys.argv[i+1]
		if opt in allowedoptions:
			options[opt]=val
		else:
			print("Unknown option:",sys.argv[i])
			sys.exit()
	except IndexError:
		# Must have reached the last item
		orig_id = sys.argv[i]
		break
	except:
		raise

if 'len' in options.keys():
	lim = int(options['len'])
else:
	lim = 32
	
_hash.update(bytes(orig_id,'utf-8'))

anon_id = str(_hash.hexdigest()[0:lim]).upper()

if 'append' in options.keys():
	anon_id += options['append']

pyperclip.copy(anon_id)
print(anon_id)
input("Hashed ID copied to clipboard")
