import sys
import hashlib
import pyperclip

outputfile = r'K:\!PY-OUTPUT\dump\id_mapper.csv'

search_id = sys.argv[1]

prefix = 'RRK'
letters = ['K','V','G','S']
#letters = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N',
#           'O','P','Q','R','S','T','U','V','W','X','Y','Z']
n_digits = 6
assume_single_match = True


max_number = 10**n_digits

mappings = []

for c in range(len(letters)):
    if len(mappings)>0 and assume_single_match:
        break
    for i in range(max_number):
        orig_id = prefix+letters[c]+str(i).zfill(n_digits)
        _hash = hashlib.new('md5')
        _hash.update(bytes(orig_id,'utf-8'))
        hash_id = str(_hash.hexdigest()[0:32]).upper()
        if hash_id==search_id:
            mappings.append([orig_id,hash_id])
            if assume_single_match:
                print('  Mapping found! Stopping...')
                break
            else:

                print('  Mapping found!')
#        mappings.append([orig_id,hash_id])
        if i%100000==0:
            print('Checking... {:.0f}%'.format((max_number*c+i)/(len(letters)*max_number)*100))

#with open(outputfile,'w') as outfile:
#    outfile.write('\n'.join([','.join(a) for a in mappings]))

if len(mappings)>0:
    if len(mappings)>1:
        print('\n{} mappings found for {}:'.format(len(mappings),search_id))
    else:
        print('\n{} mapping found for {}:'.format(len(mappings),search_id))
    for row in mappings:
        print('   {}'.format(row[0]))
else:
    print('No mappings found for {}'.format(search_id))

pyperclip.copy(mappings[0][0])
input("Hashed ID copied to clipboard")
