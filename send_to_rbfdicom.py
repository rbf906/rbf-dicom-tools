import os
import pydicom
from easygui import diropenbox
from subprocess import call

#### MAKE SURE THESE DETAILS ARE CORRECT ############
storescu_path = r'C:\Apps\dcmtk\bin\storescu.exe'
options = ' -v -aet RFPYTHON -aec RBFDICOM 90.255.243.224 4111 '

# "options" requires a leading and trailing space for concatenating
# with other components of the command
############################################

def send_all(input_path,preamble):
	file_list = os.listdir(input_path)
	for f in file_list:
		fullpath = os.path.join(input_path, f)
		#~ print '\n'+fullpath
		if not os.path.exists(fullpath):
			print( "...File doesn't exist")
			continue
		if os.path.isdir(fullpath):
			send_all(fullpath,preamble)
			print("...Directory found")
			continue

		ds = None
		try:
			ds = pydicom.dcmread(fullpath)
		except:
			print("...Not a DICOM file")
			continue
		command=preamble+fullpath
		print(command)
		call(preamble+fullpath,shell=False)
	return


if __name__=='__main__':
	inpath = diropenbox("Select input directory","INPUT","M:/")
	print(inpath)
	preamble = storescu_path+options

	yesno = input("This will send ALL DICOM DATA in this directory. Are you absolutely sure? (yes/no): ")

	if not inpath is None:
		if yesno.upper()=="YES":
			send_all(inpath,preamble)
		else:
			print("Cancelling - you didn't say yes.")
