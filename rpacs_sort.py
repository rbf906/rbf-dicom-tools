import pydicom
from easygui import diropenbox, fileopenbox
import numpy as np
import os
import sys
import shutil
from multiprocessing import Pool
from functools import partial
import re

def find_files(input_path):
    files = []
    for root, d, f in os.walk(input_path):
        for filename in f:
            files.append(os.path.join(root,filename))
    return files

def sort_file(path,output_root=r'.\sorted',mode='copy'):
    # print(mode)
    ext = ""
    ext = os.path.splitext(path)[1].upper()

    if not str(ext[1:]).isalpha() or len(ext)>5:
        ext=".DCM"

    dcm=""
    if ext==".IMA":
        dcm="_IMA"

    try:
        ds = pydicom.dcmread(path, force=True)
    except:
        print(str(path)+" is probably not a DICOM file - header could not be read")
        return None
    try:
#            print "Opened"
        # anon = test_anon(path)
# #            print "Anon checked"
        # if anon == True:
            # header = get_ascii(path)
# #                print "Got ASCII"
        if "PatientName" in ds:
            name = str(ds.PatientName).replace("^","_",1).replace("^","").replace(" ","_")
            lastname = str(ds.PatientName.family_name)
            name = re.sub('[^\w\-_\.0-9 ]', '_', name)
            lastname = re.sub('[^\w\-_\.0-9 ]', '_', lastname)
#                print "Got name"
        else:
            name = 'unknown name'
            lastname = 'unknown trial'
        if "PatientID" in ds:
            pt_ID = ds.PatientID
            pt_ID = re.sub('[^\w\-_\.0-9 ]', '_', pt_ID)
        else:
            pt_ID = "XXXXXXXX"
        if "StudyDate" in ds:
            date = ds.StudyDate
            date = date[0:4]+'-'+date[4:6]+'-'+date[6:8]
#                print "Got date"
        else:
            date = 'unknown_date'
        if "StudyTime" in ds:
            time = ds.StudyTime
#                print "Got time"
        else:
            time = "unknown_time"
        if "SeriesNumber" in ds:
            num = int(ds.SeriesNumber)
#                print "Got series"
        else:
            num = "-"
        if "SeriesDescription" in ds:
            desc = ds.SeriesDescription
            desc = re.sub('[^\w\-_\.0-9 ]', '_', desc)
#                print "Got description"
        elif "ProtocolName" in ds:
            desc = ds.ProtocolName
            desc = re.sub('[^\w\-_\.0-9 ]', '_', desc)
#                print "Got protocol name"
        else:
            desc = 'unknown'
            # for s in header:
                # if "ProtocolName" in s:
                    # tag = s.split("=")
                    # tag[1]=tag[1].strip("=").strip().strip('""').replace("+AF8-", "_").replace("+AD0-", "=")
                    # desc = tag[1]
        if "StudyInstanceUID" in ds:
            studyuid = ds.StudyInstanceUID
            studyuid = re.sub('[^\w\-_\.0-9 ]', '_', studyuid)

        else:
            studyuid = "unknown"

        try:
            if "SPECTROSCOPY" in str(ds.SOPClassUID.name()).upper() or '1.3.12.2.1107.5.9.1' in str(ds.SOPClassUID).upper():
                imtype = "SPEC_"
            elif "IMAGE" in str(ds.SOPClassUID.name()).upper():
                imtype = "IM_"

            else:
                imtype = "XX_"

        except:
            imtype = ""


        if 'InstanceNumber' in ds:
            instance = int(ds.InstanceNumber)
#                print "got instance"
        else:
            instance=0

        if 'SOPInstanceUID' in ds:
            instanceuid = str(ds.SOPInstanceUID)
            instanceuid = re.sub('[^\w\-_\.0-9 ]', '_', instanceuid)
        else:
            instanceuid = "xx"

        series_dir = str(num).zfill(3)+dcm+"_"+str(desc)#.replace('/','_').strip("=").strip().strip('""').replace("+AF8-", "_").replace("+AD0-", "=").replace(r'*',"star").replace(':','-')
        #~ print(series_dir)

        # Removed the following options so that this script always defaults to rpacs sort
        # if '--date-top' in sys.argv:
        #     new_dir = os.path.join(output_root,date+"_"+time[0:4]+'_'+studyuid[-4:],series_dir)
        # elif '--patient-top' in sys.argv:
        #     new_dir = os.path.join(output_root,name+"_"+pt_ID,date+"_"+time[0:4]+'_'+studyuid[-4:],series_dir)
        # elif '--no-id-no-date' in sys.argv:
        #     new_dir = os.path.join(output_root,name+'_UID'+studyuid[-4:],series_dir)
        # elif '--rpacs-sort' in sys.argv:
        #     new_dir = os.path.join(output_root,'RRK'+lastname,pt_ID,date+"_"+time[0:4]+'_'+studyuid[-4:],series_dir)
        # else:
        #     new_dir = os.path.join(output_root,name+"_"+pt_ID,date+"_"+time[0:4]+'_'+studyuid[-4:],series_dir)

        new_dir = os.path.join(output_root,'RRK'+lastname,pt_ID,date+"_"+time[0:4]+'_'+studyuid[-4:],series_dir)





        if not os.path.exists(new_dir):
            try:
                os.makedirs(new_dir)
                print("CREATED DIRECTORY: {}".format(new_dir))
#                    print "created directory"
            except FileExistsError:
                # Added for multiprocessing in case separate threads both try and create the directory
                pass
            except:
                print(str(new_dir)+'Error creating directory')
                raise

        new_path = os.path.join(new_dir, imtype+str(instance).zfill(5)+"_"+instanceuid[-4:]+ext)

        try:
            #print "creating path"
            if mode=='copy':
                shutil.copyfile(path,new_path)
            elif mode=='move':
                shutil.move(path,new_path)
            elif mode=='resave':
                # Force implicit VR
                ds.is_implicit_VR = True
                ds.is_little_endian = True
                ds.file_meta.TransferSyntaxUID = pydicom.uid.ImplicitVRLittleEndian
                ds.save_as(new_path)
        except:
            raise

    except:
        raise

    return new_path

def get_ascii(path):
    f = open(path, 'rb').readlines()
    startline = 0
    endline = 0
    for i in range(len(f)):
        f[i] = f[i].rstrip('\n')
        if "ASCCONV BEGIN" in f[i]:
            startline = i
        if "ASCCONV END" in f[i]:
            endline = i
    ascconv = f[startline:endline+1]
    return ascconv

def remove_empty_directories(path):
    for root,d,f in os.walk(path,topdown=False):
        for name in d:
            this_dir = os.path.join(root,name)
            try:
                if len(os.listdir(this_dir))==0:
                    try:
                        os.rmdir(this_dir)
                    except:
                        print("Couldn't delete empty directory: {}".format(this_dir))
            except:
                pass
    return





if __name__ == '__main__':
    # file_name = sys.argv[1]
    # sort_file(file_name,output_root=r'M:\RPACS_EXPORT',mode='move')

    input_path = r'..\mriphysics_aet'
    output_root=r'M:\RPACS_EXPORT'
    mode='move'

    files = []
    with open('transfers_to_do.txt','r') as file_object:
        files = file_object.read().split('\n')

    # input_path = r'.\unsorted'
    # output_root = r'.'
    #
    # mode = 'copy'
    # if '--move' in sys.argv:
    #     mode = 'move'
    # elif '--resave' in sys.argv:
    #     mode = 'resave'
    #
    # print("WALKING {} TO FIND FILES...\n".format(input_path))
    files = find_files(input_path)
    with Pool() as p:
        if mode=='move':
            print("{}ING {} FILES FROM\n    {}\nTO\n    {}\n".format(mode[0:3].upper(),len(files),input_path,output_root))
        else:
            print("{}ING {} FILES FROM\n    {}\nTO\n    {}\n".format(mode.upper(),len(files),input_path,output_root))
        p.map(partial(sort_file,output_root=output_root,mode=mode),files)

    # If move, delete all empty directories in input directory
    # if mode=='move':
    #     remove_empty_directories(input_path)


    # input("Done!  Press ENTER to exit.")
